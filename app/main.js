require.config({
    paths: {
		'angular': '../../bower_components/angular/angular',
    'ngRoute': '../../bower_components/angular-route/angular-route.min',
    'ui.bootstrap': '../../bower_components/angular-bootstrap/ui-bootstrap.min',
    'd3': '../../bower_components/d3/d3.min',
    'angular-google-gapi': '../../bower_components/angular-google-gapi/angular-google-gapi.min',
    'async': '../../bower_components/requirejs-plugins/src/async',
    'twitter': '//platform.twitter.com/widgets',
    'angular-loading-bar': '../../bower_components/angular-loading-bar/build/loading-bar.min'
    },
	shim: {
    'ngRoute': {
      deps: ['angular']
    },
    'ui.bootstrap': {
      deps: ['angular']
    },
    'angular-loading-bar' :{
      deps:['angular']
    }
	}
});

require(['app'], function() {
        'use strict';
        angular.bootstrap(document, ['app']);
    });
