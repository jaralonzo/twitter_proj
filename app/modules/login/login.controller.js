  define([], function() {
  'use strict';

  logincontroller.$inject = ['$scope', 'LoginService']

  function logincontroller($scope, LoginService) {

    var found = !!sessionStorage.activeUserId && !!sessionStorage.activeUserEmail;
    if (found) {
      window.location.href = "http://" + window.location.host + "#/home";
    }

    var vm = this;
    vm.service = LoginService;
    vm.googleSignIn = googleSignIn;
    vm.activeUser = sessionStorage.activeUser;

    function googleSignIn() {
      vm.service.googleSignIn();
      console.log("User"+ vm.activeUser);
    }
  }

  return logincontroller;

});
