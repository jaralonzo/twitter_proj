define([], function() {
  'use strict';

  loginservice.$inject = ['$http', '$q', '$location', '$window']

  function loginservice($http, $q, $location, $window, $cookies) {
    var vm = this;
    vm.googleSignIn = googleSignIn;
    vm.handleAuthResult = handleAuthResult;

    function googleSignIn() {
      //var SCOPE = 'https://www.googleapis.com/auth/userinfo.email';
      var SCOPE = 'https://www.googleapis.com/auth/plus.login';
      var RESPONSE_TYPE = 'token id_token';
      var CLIENT_ID = '77614637688-2p8pdpnk7gp0cauhf7k5hoh60gv6ef2a.apps.googleusercontent.com'

      gapi.auth.authorize({
        client_id: CLIENT_ID,
        scope: SCOPE,
        immediate: false,
        response_type: RESPONSE_TYPE
      }, handleAuthResult);
    }

    function handleAuthResult(authRes) {

      if (authRes && !authRes.error) {
        console.log('Signed in: ' + gapi.auth.getToken()['status']['signed_in']);
        gapi.client.load('plus', 'v1').then(function() {
          var request = gapi.client.plus.people.get({
            'userId': 'me'
          });
          request.execute(function(resp) {
            console.log('ID: ' + resp.id);
            console.log('Display Name: ' + resp.displayName);

            //Set sessions
            sessionStorage.activeUserId = resp.result.id;
            sessionStorage.activeUser = resp.result.displayName;
            sessionStorage.activeUserImg = resp.result.image.url;
            sessionStorage.oAuthProvider = 'GOOGLE';

            window.location.replace('#/home');
          });
        });
      }
    }
  }
  return loginservice;

});
