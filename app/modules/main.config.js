define(['twitter'], function () {
  'use strict';

  config.$inject = ['$routeProvider'];

  function config($routeProvider) {
    $routeProvider
                .when('/login', {
                  templateUrl: 'partials/login.partial.html',
                  controller: 'LoginController as lc',
                })
                .when('/home', {
                  templateUrl: 'partials/home.partial.html',
                  controller: 'MainController as mc'
                })
                .when('/overview', {
                  templateUrl: 'partials/overview.partial.html',
                })
                .otherwise({ redirectTo: '/login' });
     }
  return config;
});
