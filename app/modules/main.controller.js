define([], function() {
  'use strict';

  maincontroller.$inject = ['$scope', '$interval', 'MainService']

  function maincontroller($scope, $interval, MainService){

    $scope.startBattle = false;
    var vm = this;
    vm.service = MainService;
    vm.startBattle = startBattle;;
    vm.getByHashtagNum1 = getByHashtagNum1;
    vm.getByHashtagNum2 = getByHashtagNum2;
    vm.finished = true;
    vm.hasNextPageH1 = true;
    vm.hasNextPageH2 = false;
    vm.maxIdH1 = "-1";
    vm.maxIdH2 = "-1";
    vm.nextPageQueryH1 = "";
    vm.nextPageQueryH2 = "";
    vm.date = "2015-12-02";

    function startBattle (){
      vm.finished = false;

      vm.query1 = "#" + $scope.hashtag1 + " "+ vm.date;
      vm.query2 = "#" + $scope.hashtag2 + " "+ vm.date;

      getByHashtagNum1(vm.query1, vm.nextPageQueryH1, vm.maxIdH1 );
      //getByHashtagNum2(vm.query2, vm.nextPageQueryH2, vm.maxIdH2 );
    }

    function getByHashtagNum1 (key, nextPageQuery, maxId){
      return vm.service.getByHashtag(key, nextPageQuery, maxId).then(function(response){
        vm.tweet1 =  response.result;
        vm.finished = true;
        console.log("Key: " + key);
        console.log("nextPageQuery: "+ nextPageQuery);
        console.log("maxId: " + maxId);
        console.log(vm.tweet1);

        console.log("Before:"+vm.nextPageQueryH1);
        vm.nextPageQueryH1 = vm.tweet1.nextPageQuery;
        console.log("After: "+vm.nextPageQueryH1);
        vm.maxIdH1 = vm.tweet1.maxId;
        vm.hasNextPageH1= true;

        if (nextPageQuery == vm.nextPageQueryH1 ){
          console.log("NO MORE")
          vm.hasNextPageH1 = false;


        }

        return vm.tweet1;
      }, function(error){
        return error;
      });
    }

    function getByHashtagNum2 (key, nextPageQuery, maxId){
      return vm.service.getByHashtag(key, nextPageQuery, maxId).then(function(response){
        vm.tweet2 =  response.result;
        vm.finished = true;
        console.log("Key: " + key);
        console.log(vm.tweet2);

        if (nextPageQuery !== "" ){
            vm.hasNextPageH2 = true;
            vm.nextPageQueryH2 = vm.tweet2.nextPageQuery;
            vm.maxIdH2 = vm.tweet2.maxId;
        }

        return vm.tweet2;
      }, function(error){
        return error;
      });
    }
  }
  return maincontroller;

});
