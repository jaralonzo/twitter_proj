define([], function() {
  'use strict';

  sanitizefilter.$inject=['$sce'];

  function sanitizefilter($sce){

      return function(htmlCode){
        // console.log("htmlCode :" + htmlCode);
        // console.log("sceCode  :" + $sce.trustAsHtml(htmlCode));
        // console.log("============================================================");
        return $sce.trustAsHtml(htmlCode);
      }
  }
  return sanitizefilter;
})
