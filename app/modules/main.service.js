define([], function() {
  'use strict';

  mainservice.$inject = ['$http', '$q']

  function mainservice($http, $q){
    var vm = this;
    vm.getByHashtag = getByHashtag;

    function getByHashtag(topic, npq, max){

      var ROOT = 'http://localhost:8080/_ah/api';
      var defer = $q.defer();
      gapi.client.load('twitterzzz', 'v1', function() {
            gapi.client.twitterzzz.tweet.getByHashtag({hashtag: topic, nextPageQuery: npq, maxId: max}).execute(function(resp) {
                  defer.resolve(resp);
            }, function(response) {
                defer.reject(resp);
            });

      }, ROOT);
      return defer.promise;
    }
  }
  return mainservice;
});
